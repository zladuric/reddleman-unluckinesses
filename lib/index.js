const getCategories = require('./util/get-categories');
const getArticlesForCategory = require('./util/get-articles-for-category');
const { pluginDefaults, PLUGIN_NAME } = require('./util/constants');

const categoriesHomePageBuilder = require('./pages/categories-home-page-builder');
const categoryPageBuilder = require('./pages/category-page-builder');

/**
 * Category pages plugin itself
 * @param env {Object} wintersmith environment
 * @param callback {function} callback when setup is done
 * @returns {undefined}
 */
module.exports = function (env, callback) {
    // assign defaults any option not set in the config file
    const options = Object.assign({}, pluginDefaults, env.config[PLUGIN_NAME]);

    const CategoriesHomePage = categoriesHomePageBuilder(options, env.plugins.Page);
    const CategoryPage = categoryPageBuilder(options, env.plugins.Page);

    // register a generator, 'category' here is the content group generated content will belong to
    // i.e. contents._.category
    env.registerGenerator('category', function (contents, cb) {
        const isProd = process.env.NODE_ENV === 'prod';
        // find all categories
        const categories = getCategories(contents, options, isProd);

        // create the object that will be merged with the content tree (contents)
        const categoryData = {
            pages: {}
        };

        // Now, for each category, find its pages and add them.
        for (let cat of categories) {
            // find all articles for category
            const articles = getArticlesForCategory(contents, cat, options, isProd);

            const pagesForCategory = [];
            // populate pages. We slice all articles into `n` pages. Then we create a page with those articles.
            const numPages = Math.ceil(articles.length / options.perPage);

            for (let i = 0; i < numPages; i++) {
                const pageArticles = articles.slice(i * options.perPage, (i + 1) * options.perPage);
                pagesForCategory.push(new CategoryPage(i + 1, pageArticles, numPages, cat));
            }

            // now add references to prev/next to each page
            for (let i = 0; i < pagesForCategory.length; i++) {
                let page = pagesForCategory[i];
                page.prevPage = pagesForCategory[i - 1];
                page.nextPage = pagesForCategory[i + 1];
            }
            for (let page of pagesForCategory) {
                console.log('page', page)
                categoryData.pages[`${cat}.${page.pageNum}.page`] = page;
            } // file extension is arbitrary
        }

        // we also need a category home page
        console.log('Cat data dingsbumsquatchding', categoryData.pages)
        categoryData.pages['index.page'] = new CategoriesHomePage(categories);

        // cb with the generated contents
        return cb(null, categoryData);
    });

    // add the article helper to the environment so we can use it later
    env.helpers.getArticlesForCategory = getArticlesForCategory;
    env.helpers.getCategories = getCategories;

    // tell the plugin manager we are done
    return callback();
};
