module.exports = function categoriesHomePageBuilder(options, Page) {
  class CategoriesHomePage extends Page {
    constructor(categories) {
      super();
      this.categories = categories;
    }

    getFilename() {
      return options.categoriesHomeFilename
    }

    getView() {
      return function (env, locals, contents, templates, callback) {
        const template = templates[options.categoriesHomeTemplate];
        if (template === null) {
          return callback(new Error(`unknown categories home template '${options.template}'`));
        }
        // setup the template context
        const ctx = {
          categories: this.categories,
        };

        // extend the template context with the environment locals
        env.utils.extend(ctx, locals);

        // finally render the template
        return template.render(ctx, callback);
      }
    }
  }
  return CategoriesHomePage;
}
