const getValidArticles = require('./get-valid-articles');

/**
 * Gets articles in a single category
 * @param contents {Object} wintersmith content tree directory
 * @param category {string} category to filter
 * @param options wintersmith options
 * @param isProd {boolean} "production" build flag
 * @returns {Object[]}
 */
module.exports = function getArticlesForCategory(contents, category, options, isProd) {
  // helper that returns a list of articles found in *contents*
  // note that each article is assumed to have its own directory in the articles directory
  return getValidArticles(contents, options, isProd)
    .filter(article => article.metadata.title && article.metadata.category.toLowerCase().includes(category))
    .sort((a, b) => b.date - a.date);
};
